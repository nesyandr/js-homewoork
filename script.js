function buttonClick(){
	clearBox('result')
    var x1 = parseInt(document.getElementById('x1').value);
    var x2 = parseInt(document.getElementById('x2').value);
	
	if(Number.isNaN(x1) || Number.isNaN(x2)){
		alert("Wrong numbers");
	} else {
        var result
        if(document.getElementById('sum').checked) {
            result = sum(x1, x2);
        } else if (document.getElementById('multipl').checked){
            result = multiplication(x1, x2);
        } else {
            alert("Choose operation");
        }
		resultDiv = document.getElementById('result');
		resultDiv.append("Результат = "+ result);
	}
}

function clearBox(elementID) {
    document.getElementById(elementID).innerHTML = "";
}

function clearXX() {
        document.getElementById("x1").value = "";
        document.getElementById("x2").value = "";
}

function sum(x1, x2) {
	if(x1 > x2){
		alert("X1 should be less than X2");
	} else {
		var res = 0;
		for (var i = x1; i <= x2; i++) {
			res += i;
		}
        return res;
	} 
}

function multiplication(x1, x2) {
	if(x1 > x2){
		alert("X1 should be less than X2");
	} else {
		var res = 1;
		for (var i = x1; i <= x2; i++) {
			res *= i;
		}
        return res;
	} 
}